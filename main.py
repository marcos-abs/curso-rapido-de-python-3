#!python3.8
'''
*****
File: main.py
Project: curso-rapido-python-3-cod3r
File Created: Saturday, 06 February 2021 16:58:16
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Saturday, 08 February 2021 11:48:54
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

# print('Bem vindo!')
# import pacote.sub.arquivo

# import tipos.variaveis
# from tipos import variaveis, basicos
# import tipos.lista
# import tipos.tuplas
# import tipos.conjuntos
# import tipos.dicionario

# import operadores.unarios
# import operadores.aritmeticos
# import operadores.relacionais
# import operadores.atribuicao
# import operadores.logicos
# import operadores.ternario
# import controle.if_1
# import controle.if_2
# import controle.for_1
# import controle.while_1
import controle.outros_exemplos

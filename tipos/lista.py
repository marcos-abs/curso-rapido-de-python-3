#!python3.8
'''
*****
File: lista.py
Project: sub
File Created: Sunday, 07 February 2021 11:10:38
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Sunday, 07 February 2021 11:10:38
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

nums = [1, 2, 3]
print(type(nums))

nums.append(3)
nums.append(4)
nums.append(500)
print(len(nums))

nums[3] = 100
nums.insert(0, -200)
print(nums[6])
print(nums[-1]) # ultimo item da lista
print(nums[-2]) # penultimo item da lista
print(nums)

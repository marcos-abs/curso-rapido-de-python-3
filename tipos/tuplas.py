#!python3.8
'''
*****
File: tuplas.py
Project: tipos
File Created: Sunday, 07 February 2021 11:24:05
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Sunday, 07 February 2021 11:24:05
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

nomes = ('Ana', 'Bia', 'Gui', 'Leo', 'Ana')

print('Bia' in nomes)

print(nomes[0]) # mostra o primeiro elemento somente.
print(nomes[1:3]) # mostra todos entre o segundo e o ultimo elemento quando sabemos o total de elementos
print(nomes[1:-1]) # mostra todos entre o segundo e o ultimo elemento
print(nomes[2:]) # mostra todos a partir do 3 elemento
print(nomes[:-2]) # mostra todos até antepenultimo

# x = ('Bia') # não é uma tupla
# print(type(x))

# x = ('Bia', ) # agora sim! é uma tupla
# print(type(x))

#TODO: Parei aqui em 7. Conjuntos em 1. Introdução (não iniciei esta aula ainda)

print(type(nomes)) # mostra tipo da variavel
print(len(nomes)) # mostra tamanho da variavel
print(nomes)

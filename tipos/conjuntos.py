#!python3.8
'''
*****
File: conjuntos.py
Project: tipos
File Created: Monday, 08 February 2021 11:47:13
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Monday, 08 February 2021 11:47:13
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

# print({1, 2, 3})
print(type({1, 2, 3}))
print({1, 2, 3, 3, 3, 3, 3}) # não aceita valores duplicados.

conj = {1, 2, 3, 3, 3, 3, 3}
# print(conj[1]) # não aceita valores indexados.
print(conj)
print(len(conj))

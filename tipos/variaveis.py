#!python3.8
'''
*****
File: variaveis.py
Project: tipos
File Created: Saturday, 06 February 2021 17:27:39
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Saturday, 06 February 2021 17:27:39
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

a = 3
b = 4.4

print(a + b)

texto = "Sua idade é..."
idade = 23

# print(texto + str(idade))
print(f'{texto} {idade}')

saudacao = "bom dia "

print(3 * saudacao)

PI = 3.14       # (não existe CONSTANTES EM PYTHON!)
PI = 3.1415     # (existe somente uma convenção de utilizar como MAIÚSCULAS)

raio = float(input('Informe o raio da circunferência? '))
# area = PI * raio ** 2
area = PI * pow(raio, 2)

print(type(raio))
print(area)

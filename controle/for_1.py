#!python3.8
'''
*****
File: for_1.py
Project: controle
File Created: Tuesday, 09 February 2021 17:52:09
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Tuesday, 09 February 2021 17:52:09
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

# for i in range(10):
#     print(i)

# for i in range(1, 11):
#     print(i)

# for i in range(1, 100, 7):
#     print(i)

# for i in range(20, 0, -3):
#     print(i)

# nums = [2, 4, 6, 8]

# for n in nums:
#     print(n)

# texto = 'Python é muito massa!'

# for letra in texto:
#     print(letra, end=' ')

# for n in {1, 2, 3, 4, 4, 4}:
#     print(n, end=' ')

produto = {
    'nome': 'Caneta',
    'preço': 8.80,
    'desconto': 0.5
}

# for atrib in produto:
#     print(atrib, '==>', produto[atrib])

# for atrib, valor in produto.items():
#     print(atrib, '==>', valor)

# for valor in produto.values():
#     print(valor, end=' ')

for atrib in produto.keys():
    print(atrib, end=' ')

print(' ')

#!python3.8
'''
*****
File: if_1.py
Project: controle
File Created: Tuesday, 09 February 2021 17:27:16
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Tuesday, 09 February 2021 17:27:16
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

nota = float(input('Informe a nota do aluno: '))
comportado = True if input('Comportado (y/n)?: ') == 'y' else False

if nota >= 9 and comportado:
    print('Duas palavras: para bens! :P')
    print('Quadro de Honra')
elif nota >= 7:
    print('Aprovado')
elif nota >= 5.5:
    print('Recuperação')
elif nota >= 3.5:
    print('Recuperação + Trabalho')
else:
    print('Reprovado')
print(nota)

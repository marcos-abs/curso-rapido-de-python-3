#!python3.8
'''
*****
File: if_2.py
Project: controle
File Created: Tuesday, 09 February 2021 17:40:13
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Tuesday, 09 February 2021 17:40:13
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

a = 'valor'     # Existe!!!
a = 0           # Não existe!!!
a = -1          # Existe!!!
a = -0.000001   # Existe!!!
a = ''          # Não existe!!!
a = ' '         # Existe!!!
a = []          # Não existe!!!
a = {}          # Não existe!!!

# print(not 'valor')
# print(not not 'valor')

if a:
    print('Existe!!!')
else:
    print('Não existe ou zero ou vazio...')

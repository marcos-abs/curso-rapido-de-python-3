#!python3.8
'''
*****
File: outros_exemplos.py
Project: controle
File Created: Wednesday, 10 February 2021 10:34:57
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Wednesday, 10 February 2021 10:34:57
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

pessoas = ['Gui', 'Rebeca']
adjs = ['Sapeca', 'Inteligente']

for p in pessoas:
    for a in adjs:
        print(f'{p} é {a}')

for i in [1, 2, 3]:
    pass

for i in range(1, 11):
    if i % 2:
        continue
    print(i)

for i in range(1, 11):
    if i == 5:
        break
    print(i)

print('Fim!')

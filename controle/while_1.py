#!python3.8
'''
*****
File: while_1.py
Project: controle
File Created: Wednesday, 10 February 2021 10:02:13
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Wednesday, 10 February 2021 10:02:14
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''
x = 10

while x:
    print(x, end=' ')
    x -= 1

print(' ')
print('Fim!')

# total = 0
# qtde = 0
# nota = 0

# while nota != -1:
#     nota = float(input('Informe a nota ou -1 para sair: '))
#     if nota != -1:
#         qtde += 1
#         total += nota

# print(f'A média da turma é {total / qtde}')

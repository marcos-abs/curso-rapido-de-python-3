#!python3.8
'''
*****
File: logicos.py
Project: operadores
File Created: Tuesday, 09 February 2021 17:11:53
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Tuesday, 09 February 2021 17:11:53
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

b1 = True # primeira letra maiúscula
b2 = False # primeira letra maiúscula
b3 = True # primeira letra maiúscula

print(b1 and b2 and b3) # false
print(b1 or b2 or b3) # true
print(b1 != b2) # true

print(not b1) # false
print(not b2) # true

print(b1 and not b2 and b3) # true

x = 3
y = 4

print(b1 and not b2 and x < y) # true

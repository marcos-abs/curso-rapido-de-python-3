#!python3.8
'''
*****
File: aritmeticos.py
Project: operadores
File Created: Tuesday, 09 February 2021 16:44:34
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Tuesday, 09 February 2021 16:44:34
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''
x = 10
y = 3

print(x + y)
print(x - y)
print(x * y)
print(x / y)
print(x % y)

par = 34
impar = 33
print(par % 2 == 0)
print(impar % 2 == 1)
#!python3.8
'''
*****
File: atribuicao.py
Project: operadores
File Created: Tuesday, 09 February 2021 16:58:22
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Tuesday, 09 February 2021 16:58:23
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

resultado = 1
print(resultado)

resultado += resultado # resultado = resultado + resultado = 2
print(resultado)

resultado += 3 # resultado = resultado + 3 = 5
print(resultado)

resultado -= 1 # resultado = resultado - 1 = 4
print(resultado)

resultado *= 4 # resultado = resultado * 4 = 16
print(resultado)

resultado /= 2 # resultado = resultado / 2 = 8
print(resultado)

resultado %= 6 # resultado = resultado % 6 = 2  ## (resto da divisão 8 / 6 = 2)
print(resultado)

#!python3.8
'''
*****
File: unarios.py
Project: operadores
File Created: Monday, 08 February 2021 12:02:59
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Monday, 08 February 2021 12:02:59
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''
y = 4

print(not False)
print(not True)
print(--3)
print(--y)
print(+3) # sem modificação do resultado

w = 12
# w++ # não existe em Python
print(w)
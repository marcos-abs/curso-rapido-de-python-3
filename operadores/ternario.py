#!python3.8
'''
*****
File: ternario.py
Project: operadores
File Created: Tuesday, 09 February 2021 17:20:25
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Tuesday, 09 February 2021 17:20:25
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

lockdown = False
grana = 30

status = 'Em casa' if lockdown or grana <= 100 else 'Uhhuuuu'

print(f'O status é: {status}')

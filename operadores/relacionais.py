#!python3.8
'''
*****
File: relacionais.py
Project: operadores
File Created: Tuesday, 09 February 2021 16:51:24
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Tuesday, 09 February 2021 16:51:25
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

x = 7
y = 5

print(x > y)
print(x >= y)
print(x < y)
print(x <= y)
print(x == y)
# print(x === y) # não existe em Python
print(x != y)


print('5' != 5) # em javascript são iguais, em Python não
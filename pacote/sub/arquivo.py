#!python3.8
'''
*****
File: arquivo.py
Project: sub
File Created: Saturday, 06 February 2021 17:12:09
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Saturday, 06 February 2021 17:12:09
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

print(__name__)
print(__package__)

print(abs(-321))

#!python3.8
'''
*****
File: basicos.py
Project: sub
File Created: Saturday, 06 February 2021 18:21:33
Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
-----
Last Modified: Saturday, 06 February 2021 18:21:33
Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
-----
Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
-----
Description:

*****
'''

print(type(1))
print(type(1.1))
print(type('texto'))
print(type(False))
print(type(True))
